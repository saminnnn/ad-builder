"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
var path_1 = require("path");
var fs = require('fs')
var mainWindow;

function createWindow() {
    // Create the browser window.
    mainWindow = new electron_1.BrowserWindow({
        height: 800,
        webPreferences: {
            preload: (0, path_1.join)(__dirname, 'dist', "preload.js")
        },
        width: 1200
    });
    // and load the index.html of the app.
    mainWindow.loadFile((0, path_1.join)(__dirname, "index.html"));
    // Open the DevTools.
    mainWindow.webContents.openDevTools();
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
electron_1.app.on("ready", function () {
    createWindow();
    electron_1.app.on("activate", function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (electron_1.BrowserWindow.getAllWindows().length === 0)
            createWindow();
        
        
    });
});
// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
electron_1.app.on("window-all-closed", function () {
    if (process.platform !== "darwin") {
        electron_1.app.quit();
    }
});
// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
//# sourceMappingURL=main.js.map


electron_1.ipcMain.on("toMain", (event, args) => {
    const path = path_1.join('output', 'variables.js')
    fs.writeFile(path, args, (e) => {
        if(e) console.log(e);
    })
    
});

electron_1.ipcMain.on('copyFile', (event, args) => {
    electron_1.dialog.showOpenDialog({
        filters: [{name: 'Images', extensions: ['jpg', 'jpeg', 'png']}], properties: ['openFile']
    }).then(value => {
        if(!value.canceled) {
            const destination = path_1.join('generic_dynamic_ad', 'images', 'f' + args[0] + '_' + args[1]);
            fs.cp(value.filePaths[0], destination, (e) => {
                if(!e) {
                    console.log(args)
                    mainWindow.send('filepath', args);
                }
            });
            
        }
        
    });
})



  