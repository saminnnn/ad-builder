//test values for control variables
//red|210,200|5,50|rb for text
//10,10|red|blue|yellow for cta

var adWidth = 300, adHeight = 250, totalFrameCount = 7, 
    frames = [], frameDelays = [], directions = [],
    stye = document.getElementById('style');

// preload();

function preload() {
    directions = [];
    style.innerHTML = variables.insertCSS; //custom css
    loadImages(variables)//load images

    //set the frames
    if(!variables.Frame_Select) frames = ['1'];
    else frames = variables.Frame_Select.split(',');

    frameDelays = ('0,' + variables.Frame_Durations).split(',');
    // directions = variables.Animation_Direction.split(',');

    for(var i = 0; i < frames.length; i++) {
        frames[i] = parseInt(frames[i]);
        frameDelays[i + 1] = parseInt(frameDelays[i + 1]);
        if(!frameDelays[i + 1]) frameDelays[i + 1] = 2;
    }

    var ctaLink = variables.CTA_clickthrough_URL;
    if(!ctaLink) ctaLink = variables.BG_clickThrough_URL;

    var animationDirections = {}
    if(variables.Animation_Direction) {
        animationDirections = JSON.parse(variables.Animation_Direction);
    }

    for(let i = 1; i <= totalFrameCount; i++) {
        var copy = variables['F' + i + '_Copy'];
        var copyControl = variables['F' + i + '_Copy_Color_WH_XY_Align'];
        var ctaText = variables['F' + i + '_CTA_Text'];
        var ctaControl = variables['F' + i + '_CTA_xy_fgColor_bgColor_roColor'];
        var copyText = document.getElementById('copytext' + i);
        var copyArea = document.getElementById('copyArea' + i);
        var cta = document.getElementById('cta' + i);

        setCopyCta(copy, copyControl, ctaText, ctaControl, copyText, copyArea, cta);

    }

    for(var i = 1; i <= totalFrameCount; i++) {
        if(frames.indexOf(i) == -1) {
            continue;
        }

        var currentFrameAnimation = animationDirections['F' + i];
        if(!currentFrameAnimation) currentFrameAnimation = {Background_Image: null, Logo: null, Overlay_Image: null, Copy: null, CTA_Text: null};
        
        var processedObject = {bg: null, text: null, overlay: null, logo: null, cta: null};
        processedObject.bg = getAnimationType(currentFrameAnimation.Background_Image);
        processedObject.text = getAnimationType(currentFrameAnimation.Copy);
        processedObject.overlay = getAnimationType(currentFrameAnimation.Overlay_Image);
        processedObject.logo = getAnimationType(currentFrameAnimation.Logo);
        processedObject.cta = getAnimationType(currentFrameAnimation.CTA_Text);
        

        setInitialFrameState(document.getElementById('bg' + i), processedObject.bg.inType);
        setInitialFrameState(document.getElementById('logo' + i), processedObject.logo.inType);
        setInitialFrameState(document.getElementById('overlay' + i), processedObject.overlay.inType);
        setInitialFrameState(document.getElementById('copyArea' + i), processedObject.text.inType);
        setInitialFrameState(document.getElementById('cta' + i), processedObject.cta.inType);

        directions.push(processedObject);
        // directionIndex++;
    }
    frameDelays[0] = 0;//there should not be any delay when displaying the first frame
        
    // console.log(directions)
    
}

function setCopyCta(headline, headlineAttributes, ctaText, ctaAttributes, copytext, copyarea, cta) {
    copytext.innerHTML = headline;
    var attributes = getAttributes(headlineAttributes);
    setAttributes(copyarea, attributes, copytext)

    if(ctaText) {
        cta.style.display = 'flex';
        cta.innerHTML = ctaText;

        var ctaProps = getCtaProps(ctaAttributes);
        setCtaProps(cta, ctaProps);
    }
}

function loadImages(variables) {
    var imageCount = totalFrameCount * 3, imageLoaded = 0;

    for(let i = 1; i <= totalFrameCount; i++) {
        const bg = document.getElementById('bg' + i);
        bg.src = variables['F' + i + '_Background_Image'];
        bg.addEventListener('load', iLoad, false);

        const logo = document.getElementById('logo' + i);
        logo.src = variables['F' + i + '_Logo'];
        logo.addEventListener('load', iLoad, false);

        const overlay = document.getElementById('overlay' + i);
        overlay.src = variables['F' + i + '_Overlay_Image'];
        overlay.addEventListener('load', iLoad, false);
    }

    function iLoad() {
        imageLoaded++;
        if(imageLoaded == imageCount) {
            start();
        }
    }
}

function setInitialFrameState(frame, state){
    if(!state || state == 'fade' || state == 'pop') frame.style.opacity = 0;
    else if(state == 'top') frame.style.top = (-1 * adHeight) + 'px';
    else if(state == 'bottom') frame.style.top = ( adHeight) + 'px';
    else if(state == 'left') frame.style.left = (-1 * adWidth) + 'px';
    else if(state == 'right') frame.style.left = (adWidth) + 'px';
    else if(state == 'none') frame.style.display = 'none';
    else if(state == 'grow') {
        frame.style.transform = 'scale(.5)';
        frame.style.opacity = 0;
    } else if(state == 'drop') {
        frame.style.opacity = 0;
        frame.style.transform = 'scale(2)';
    }
}

function getAnimationType(value) {
    
    var returnObject = {inDelay: 0, inType: 'fade', outDelay: 0, outType: 'none'}
    if(!value) return returnObject;

    var typeList = value.split(',');

    if(typeList[0]) returnObject.inDelay = parseFloat(typeList[0]);
    if(typeList[1]) returnObject.inType = typeList[1].trim().toLowerCase();
    if(typeList[2]) returnObject.outDelay = parseFloat (typeList[2]);
    if(typeList[3]) returnObject.outType = typeList[3].trim().toLowerCase();

    return returnObject;
}

function getAttributes(variable) {
    //F1_Copy_Color_WH_XY_Align
    var returnArray = [null, 0, 0, 0, 0, null, null]
    
    var variableList = variable.split('|');

    if(variableList[0]) returnArray[0] = variableList[0];
    if(variableList[1]) {
        var whList = variableList[1].split(',')
        returnArray[1] = whList[0];
        returnArray[2] = whList[1];
    }
    if(variableList[2]) {
        var xyList = variableList[2].split(',')
        returnArray[3] = xyList[0];
        returnArray[4] = xyList[1];
    }
    if(variableList[3]) {
        returnArray[5] = variableList[3];
    }
    if(variableList[4]) {
        returnArray[6] = variableList[4]
    }

    return returnArray;
}

function setAttributes(element, attributeList, innerItem) {//set attributes when available
    if(!attributeList) return;

    if(attributeList[0]) element.style.color = attributeList[0];
    if(attributeList[1]) {
        element.style.width = attributeList[1] + 'px';
        innerItem.style.width = attributeList[1] + 'px';
    }
    if(attributeList[2]) element.style.height = attributeList[2] + 'px';
    if(attributeList[3]) element.style.left = attributeList[3] + 'px';
    if(attributeList[4]) element.style.top = attributeList[4] + 'px';
    
    if(attributeList[5]) {
        //clr, tcb
        attributeList[5] = attributeList[5].toLowerCase();
        var hoizontal = attributeList[5].substring(0, 1);
        var vertical = attributeList[5].substring(1);

        if(hoizontal == 'l') {
            innerItem.style.textAlign = 'left'
        } else if(hoizontal == 'c') {
            innerItem.style.textAlign = 'center'
        } else if(hoizontal == 'r') {
            innerItem.style.textAlign = 'right';
        }

        if(vertical == 't') {
            innerItem.style.top = '0px'
        } else if(vertical == 'c') {
            var innerElementHeight = innerItem.offsetHeight;
            var outerHeight = element.offsetHeight;
            var top = (outerHeight - innerElementHeight) / 2;
            innerItem.style.top = top + 'px';
        } else if(vertical == 'b') {
            innerItem.style.bottom = '0px';
        }
 
    }

    if(attributeList[6]) innerItem.style.fontSize = attributeList[6] + 'px';
}

function getCtaProps(ctaProps) {
    //xy_fgColor_bgColor_roColor
    var returnArray = [0, 0, null, null, null];
    var ctaPropList = ctaProps.split('|');

    if(ctaPropList[0]) {
        var xy = ctaPropList[0].split(',');
        returnArray[0] = xy[0];
        returnArray[1] = xy[1];
    }
    if(ctaPropList[1]) returnArray[2] = ctaPropList[1];
    if(ctaPropList[2]) returnArray[3] = ctaPropList[2];
    if(ctaPropList[3]) returnArray[4] = ctaPropList[3];

    return returnArray;
}

function setCtaProps(cta, params) {
    //xy_fgColor_bgColor_roColor
    if(params[0]) cta.style.left = params[0] + 'px';
    if(params[1]) cta.style.top = params[1] + 'px';
    if(params[2]) cta.style.color = params[2];
    if(params[3]) cta.style.backgroundColor = params[3];

    setTimeout(function() {
        var backgroundColor = getCssValue(cta, 'backgroundColor');
        if(params[4]) {
            cta.addEventListener('mouseover', function() {
                cta.style.backgroundColor = params[4];
            });
    
            cta.addEventListener('mouseout', function() {
                cta.style.backgroundColor = backgroundColor;
            });
        }    
    
    }, 500);
    
       
}
// ---------------------ANIMATIONS

function start() {
    var tl = new TimelineMax();
    tl.set('.frame', {display: 'none'});
    tl.set('.frame', {opacity: 1});
    
    var delayIndex = 0;

    for(var i = 1; i <= totalFrameCount; i++) {
        if(frames.indexOf(i) == -1) {
            continue;
        }

        tl.set('#frame' + i, {display: 'block'});
        var currentDirection = directions[delayIndex];
        var currentLabel = 'frame' + i;
        tl.addLabel(currentLabel);
        
        createInAnimation(tl, currentDirection.bg.inType, '#bg' + i, 0, 0, currentDirection.bg.inDelay, currentLabel);
        createInAnimation(tl, currentDirection.logo.inType, '#logo' + i, 0, 0, currentDirection.logo.inDelay, currentLabel);
        createInAnimation(tl, currentDirection.overlay.inType, '#overlay' + i, 0, 0, currentDirection.overlay.inDelay, currentLabel);


        var copyAttributes = getAttributes(variables['F' + i + '_Copy_Color_WH_XY_Align']);
        createInAnimation(tl, currentDirection.text.inType, '#copyArea' + i, copyAttributes[3], copyAttributes[4], currentDirection.text.inDelay, currentLabel);

        if(variables['F' + i + '_CTA_Text']) {
            var ctaAttributes = getCtaProps(variables['F' + i + '_CTA_xy_fgColor_bgColor_roColor']);
            createInAnimation(tl, currentDirection.cta.inType, '#cta' + i, ctaAttributes[0], ctaAttributes[1], currentDirection.cta.inDelay, currentLabel);
        }

        currentLabel = 'inDone' + i;
        tl.addLabel(currentLabel);
        
        tl.set('#frame' + i, {opacity: 1, delay: frameDelays[delayIndex + 1]}, currentLabel)//empty timeline point incase there are no out animation
        
        //out animation
        createOutAnimation(currentDirection.bg.outType, currentDirection.bg.outDelay + frameDelays[delayIndex + 1], 
            '#bg' + i, currentLabel, tl);
        createOutAnimation(currentDirection.logo.outType, currentDirection.logo.outDelay + frameDelays[delayIndex + 1], 
            '#logo' + i, currentLabel, tl);
        createOutAnimation(currentDirection.overlay.outType, currentDirection.overlay.outDelay + frameDelays[delayIndex + 1], 
            '#overlay' + i, currentLabel, tl);
        createOutAnimation(currentDirection.text.outType, currentDirection.text.outDelay + frameDelays[delayIndex + 1], 
            '#copyArea' + i, currentLabel, tl);
        if(variables['F' + i + '_CTA_Text']) {
            createOutAnimation(currentDirection.cta.outType, currentDirection.cta.outDelay + frameDelays[delayIndex + 1], 
                '#cta' + i, currentLabel, tl);
        }

        delayIndex++;
    }
}

function createInAnimation(tl, inType, div, left, top, delay, label) {
    if(inType == 'grow') {
        tl.to(div, .5, {autoAlpha:1, ease: Back.easeOut.config(4), scale:1, delay: delay}, label)
    } 
    else if(inType == 'drop') {
        tl.to(div, .5, {autoAlpha:1, ease: Bounce.easeOut, scale:1, delay: delay}, label)
    }
    else if(inType != 'pop') {
        tl.to(div, .5, {opacity: 1, left:left, top: top, delay: delay}, label);
    } else {
        tl.set(div, {opacity: 1, delay: delay}, label);
    }
}

function createOutAnimation(outType, delay, div, label, tl) {
    if(outType == 'none') {
        return;
    }

    var outObject = {
        pop: {opacity: 0}, fade: {opacity: 0}, 
        left: {left: -1 * adWidth}, right: {left: adWidth}, top: {top: -1 * adHeight}, bottom: {top: adHeight},
        grow: {scale: .5, opacity: 0}, drop: {scale: 2, opacity: 0}
    }

    var currentOut = outObject[outType];//create object for tl
    currentOut.delay = delay;

    if(outType != 'pop') {
        tl.to(div, .5, currentOut, label);
    } else {
        tl.set(div, currentOut, label);
    }
}

function getCssValue(element, valueType) {//get style value of valuetype
    var valueText = window.getComputedStyle(element)[valueType];
    return valueText;
}

function checkPlatform() {


        try {
            var a = new Array();
    
            if (navigator.platform.toLowerCase().indexOf("mac") > -1) {
                a[0] = "macOS";
            } else if (navigator.platform.toLowerCase().indexOf("win") > -1) {
                a[0] = "windows";
            } else {
                if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
                    a[0] = "iOS";
                } else if (navigator.userAgent.match(/Opera Mini/i)) {
                    a[0] = "opera";
                } else if (navigator.userAgent.match(/Android/i)) {
                    a[0] = "android";
                } else if (navigator.userAgent.match(/BlackBerry/i)) {
                    a[0] = "BlackBerry";
                } else if (navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i)) {
                    a[0] = "WindowsPhone";
                }
            }
    
            var MSIE = window.navigator.userAgent.indexOf("MSIE ");
            var Edge = window.navigator.userAgent.indexOf("Edge/");
            var Trdt = window.navigator.userAgent.indexOf("Trident/");
    
            if (navigator.userAgent.toLowerCase().indexOf("chrome") > -1) {
                a[1] = "chrome";
            } else if (navigator.userAgent.toLowerCase().indexOf("firefox") > -1) {
                a[1] = "firefox";
            } else if (navigator.vendor && navigator.vendor.toLowerCase().indexOf("apple") > -1) {
                a[1] = "safari";
            } else if (MSIE > 0 || Edge > 0 || Trdt > 0) {
                a[1] = "IE";
            }
            return a;
    
        } catch (error) {
            console.error("Error on checkPlatform(): " + error.message);
        }
    }