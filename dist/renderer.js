var headlineContents = "", ctaContents = "", imageContents = '', animationContent = '', noOfFrames = 1;
for (var i = 1; i <= 7; i++) {
    ctaContents += `<span id="f` + i + `CTAContent"><p class="frame` + i + ` heading">
        Frame ` + i + ` CTA &nbsp;&nbsp;&nbsp; <span class="hide">hide</span></p>
        <span class="name frame` + i + `" >Frame `+ i + ` CTA text</span><br>
        <input type="text" id="F` + i + `CTACopy" class="frame` + i + ` headline" value="Learn More"><br>
        CTA Text Color: <input type="color" id="f` + i + `ctaTXTColor"><br> CTA Background Color: <input type="color" id="f` + i + `ctaBGColor" value="#808080"><br>
        CTA Roll Color: <input type="color" id="f` + i + `ctaRollColor" value="#987391"><br>
        x: <input type=\"number\" class=\"frame` + i + ` x\" id=\"f` + i + `CTAx\" value=\"179\">
        y: <input type=\"number\" class=\"frame` + i + ` y\" id=\"f` + i + `CTAy\" value=\"206\">
        </span>`;
    /*
    imageContents += `<div id="f` + i + `images"> Frame ` + i + ` Images<label for="f` + i + `bginput"><div class="imagearea frame` + i + `">
        <div class="imageinfo">Background Image<br><span id="f` + i + `bgFile" class="imageDescription">Click to Upload Image</span>
        <input type="file" class="fileinput" id="f` + i + `bginput" accept=".jpg, .jpeg, .png" onchange="imageSelected(` + i + `, 'bg', '_Background_Image')">
        </div><img id="f` + i + `bgpreview" alt="No image selected yet" class="uploadeimage"></div></label><label for="f` + i + `Overlayinput">
        <div class="imagearea frame` + i + `"><div class="imageinfo">Overlay Image<br><span id="f` + i + `OverlayFile" class="imageDescription">Click to Upload Image
        </span><input type="file" class="fileinput" id="f` + i + `Overlayinput" accept=".jpg, .jpeg, .png" onchange="imageSelected(` + i + `, 'Overlay', '_Overlay_Image')">
        </div><img id="f` + i + `Overlaypreview" alt="No image selected yet" class="uploadeimage"></div></label><label for="f` + i + `Logoinput">
        <div class="imagearea frame` + i + `"><div class="imageinfo">Logo Image<br><span id="f` + i + `LogoFile" class="imageDescription">Click to Upload Image</span>
        <input type="file" class="fileinput" id="f` + i + `Logoinput" accept=".jpg, .jpeg, .png" onchange="imageSelected(` + i + `, 'Logo', '_Logo')"></div>
        <img id="f` + i + `Logopreview" alt="No image selected yet" class="uploadeimage"></div></label></div>`*/
    imageContents += `<div id="f` + i + `images"> Frame ` + i + ` Images<div class="imagearea frame` + i + `" onclick="electronUpload(` + i + `, 'bg', '_Background_Image')">
        <div class="imageinfo">Background Image<br><span id="f` + i + `bgFile" class="imageDescription">Click to Upload Image</span>
        </div><img id="f` + i + `bgpreview" alt="No image selected yet" class="uploadeimage"></div>
        <div class="imagearea frame` + i + `"><div class="imageinfo">Overlay Image<br><span id="f` + i + `OverlayFile" class="imageDescription">Click to Upload Image
        </span>
        </div><img id="f` + i + `Overlaypreview" alt="No image selected yet" class="uploadeimage"></div>
        <div class="imagearea frame` + i + `"><div class="imageinfo">Logo Image<br><span id="f` + i + `LogoFile" class="imageDescription">Click to Upload Image</span>
        </div>
        <img id="f` + i + `Logopreview" alt="No image selected yet" class="uploadeimage"></div></div>`

    headlineContents += `<span id="frame` + i + `Content" class="framecontent"><p class="frame` + i + ` heading">Frame ` + i + ` Contents &nbsp;&nbsp;&nbsp;
        <span class="hide">hide</span></p><span class=\"name frame` + i + `\" >Frame ` + i + ` Headline</span><br>
        <input type=\"text\" class=\"frame` + i + ` headline\" id="f` + i + `headline" value=\"Frame` + i + ` headline content\"><br>Width: 
        <input type=\"number\" class=\"frame` + i + ` width\" id=\"f` + i + `Width\" value=\"300\"> Horizental Align <select id=\"f` + i + `hAlign\">
        <option value=\"left\">Left</option><option value=\"center\" selected>Center</option><option value=\"right\">Right</option></select><br>
        height: <input type=\"number\" class=\"frame` + i + ` height\" id=\"f` + i + `Height\" value=100>Vertical Align <select id=\"f` + i + `vAlign\">
        <option value=\"top\" selected>Top</option><option value=\"center\">Center</option><option value=\"bottom\">Bottom</option></select><br>
        x: <input type=\"number\" class=\"frame` + i + ` x\" id=\"f` + i + `x\" value=\"0\"> y: <input type=\"number\" class=\"frame` + i + ` y\" id=\"f` + i + `y\" 
        value=\"200\"> Font: <input type=\"number\" class=\"frame` + i + ` font\" id=\"f` + i + `Font\" value=\"18\"><br>
        Text Color: <input type="color" id="f` + i + `headlineColor" class="frame` + i + `"></span>`;

    animationContent += `<div id="f` + i + `Times"> <p>Frame ` + i + ` Timing</p> Frame Duration <input type="number" value="2" id="f` + i + `FrameTime"><br>`
        + getFrameAnimationControlHTML(i, "Headline") + getFrameAnimationControlHTML(i, 'Logo') + getFrameAnimationControlHTML(i, 'Overlay')
        + getFrameAnimationControlHTML(i, 'CTA') + getFrameAnimationControlHTML(i, 'Background', true) + '</div>';
}


// window.api.receive("fromMain", (data) => {
//     console.log(`Received ${data} from main process`);
// });
// window.api.send("toMain", "some data");

function getFrameAnimationControlHTML(frame, type, bg = false) {
    const classValue = type.toLowerCase();
    let options = `<option value="left">From Left</option><option value="right">From Right</option>
    <option value="top">From Top</option><option value="bottom">From Bottom</option>`;
    if(!bg) {
        options += '<option value="drop">Drop</option><option value="grow">Grow</option>'
    }
    options += '<option value="pop">Pop</option>'

    return `<p class="timeParagraph">` + type + ` in animation <select id="f` + frame + classValue + `AnimationIn" class="animationInput"><option value="fade" selected>Fade</option>`
        + options + `</select> in delay <input type="number" id="f` + i + classValue + `In" class="delayInput" value="0"> <br>
        ` + type + `  out animation <select id="f`+ frame + classValue + `AnimationOut" class="animationInput">
        <option value="fade">Fade</option>` + options +  `<option value="none" selected>None</option> </select> out delay <input class="delayInput" type="number" id="f` + frame + classValue + `Out" value="0"></p>`;

}

headlineContents += '<br>*All values uses pixel unit';
headlineContent.innerHTML = headlineContents;

ctaContents += '<br>*Positon values are in pixles'
ctaContent.innerHTML = ctaContents;
imageContent.innerHTML = imageContents;

animationContent += '<br>*All time values are in seconds'
timeContent.innerHTML = animationContent;

setTimeout(() => adjustVisibleFrameInfo(1), 500);

var noOfFrame = document.getElementById('frameNumber');
noOfFrame.addEventListener('change', function (e) {
    var value = e.target.value;
    if (!value) {
        showAlert('frame1Alert', 'Enter Valid Value');
        return;
    }
    var frameCount = Number.parseInt(value);
    if (!frameCount || frameCount < 1 || frameCount > 7) {
        showAlert('frame1Alert', 'Value should be between 1 and 7');
        return;
    }
    adjustVisibleFrameInfo(frameCount);
});

function adjustVisibleFrameInfo(frameCount) {
    for (var i = 1; i <= frameCount; i++) {
        var id = 'frame' + i + 'Content';
        var ctaId = 'f' + i + 'CTAContent';
        var imageId = 'f' + i + 'images';
        var animationId = 'f' + i + 'Times';
        document.getElementById(id).style.display = 'block';
        document.getElementById(ctaId).style.display = 'block';
        document.getElementById(imageId).style.display = 'block';
        document.getElementById(animationId).style.display = 'block';

    }
    for (var i = frameCount + 1; i <= 7; i++) {
        var id = 'frame' + i + 'Content';
        var ctaId = 'f' + i + 'CTAContent';
        var imageId = 'f' + i + 'images';
        var animationId = 'f' + i + 'Times';

        document.getElementById(id).style.display = 'none';
        document.getElementById(ctaId).style.display = 'none';
        document.getElementById(imageId).style.display = 'none';
        document.getElementById(animationId).style.display = 'none';
    }

    const frames = [];
    for(let i = 1; i < frameCount; i++) {
        frames.push(i)
    }
    variables.Frame_Select = frames.join(',');
    noOfFrame = frameCount
    replay();
}

function showAlert(elemntId, message) {
    var alertElement = document.getElementById(elemntId);
    alertElement.style.visibility = 'visible';
    alertElement.textContent = message;
}

function replay() {
    //#003057|300,200|0,145|ct headline
    //F1_CTA_xy_fgColor_bgColor_roColor
    const frames = [], timing = [], animation = [];
    for(let i = 1; i <= noOfFrame; i++) {
        frames.push(i);
        variables['F' + i + '_Copy'] = document.getElementById(`f` + i + `headline`).value;
        variables['F' + i + '_CTA_Text'] = document.getElementById(`F` + i + `CTACopy`).value;

        const controlValues = [
            `f` + i + `headlineColor`, `f` + i + `Width`, `f` + i + `Height`, `f` + i + `x`, `f` + i + `y`, 
            `f` + i + `hAlign`, `f` + i + `vAlign`, `f` + i + `Font`
        ].map(id => document.getElementById(id).value);
        controlValues[5] = controlValues[5].substring(0, 1);
        controlValues[6] = controlValues[6].substring(0, 1);
        variables['F' + i + '_Copy_Color_WH_XY_Align'] = controlValues[0] + '|' + controlValues[1] + ',' + controlValues[2] + '|' 
            + controlValues[3] + ',' + controlValues[4] + '|' + controlValues[5] + controlValues[6] + '|' + controlValues[7]; 
        
        const ctaValues = [
            `f` + i + `CTAx`, `f` + i + `CTAy`, `f` + i + `ctaTXTColor`, `f` + i + `ctaBGColor`, `f` + i + `ctaRollColor`
        ].map(id => document.getElementById(id).value);
        variables['F' + i + '_CTA_xy_fgColor_bgColor_roColor'] = ctaValues[0] + ',' + ctaValues[1] + '|' + ctaValues[2] + '|' + ctaValues[3] + '|' + ctaValues[4];

        timing.push(document.getElementById('f' + i + 'FrameTime').value);


    }

    const animationDirection = {};
    for(let i =1; i <= 7; i++ ) {       
        animationDirection['F' + i] = {
            Background_Image: getAnimationDirection(i, 'background'),
            Logo: getAnimationDirection(i, 'logo'),
            Overlay_Image: getAnimationDirection(i, 'overlay'),
            CTA_Text: getAnimationDirection(i, 'cta'),
            Copy: getAnimationDirection(i, 'headline')
        }
    }

    variables.Frame_Select = frames.join(',');
    variables.Frame_Durations = timing.join(',');
    variables.Animation_Direction = JSON.stringify(animationDirection)
    // console.log(variables.Animation_Direction);
    preload();   
}


function getAnimationDirection(frame, classValue) {
    return document.getElementById(`f` + frame + classValue + `In`).value + ',' 
        + document.getElementById(`f` + frame + classValue + `AnimationIn`).value + ','
        + document.getElementById(`f` + frame + classValue + `Out`).value + ','
        + document.getElementById(`f` + frame + classValue + `AnimationOut`).value;
    
}


function tabClick(index) {
    const tabs = document.getElementsByClassName('tab');
    const tabAreas = document.getElementsByClassName('contentarea');

    for(let i = 0; i < tabs.length; i++) {
        if(i + 1 == index) {
            tabs[i].classList.add('activetab');
            tabAreas[i].style.display = 'block';
        } else {
            tabs[i].classList.remove('activetab');
            tabAreas[i].style.display = 'none';
        }
    }
}

function imageSelected(frame, type, variableSuffix) {
    const input = document.getElementById('f' + frame + type + 'input')
    const preview = document.getElementById('f' + frame + type + 'preview');
    const fileInfo = document.getElementById('f' + frame + type + 'File')
    
    const files = input.files;
    const reader = new FileReader();
    reader.addEventListener('load', () => {
        const uploadedImage = reader.result;
        preview.src = uploadedImage;
        variables['F' + frames + variableSuffix] = uploadedImage; 

        fileInfo.innerText = files[0].name + ' (' + (files[0].size / 1024).toFixed(2) + 'kb)'; 
    });

    reader.readAsDataURL(files[0]);    
}

function electronUpload(frame, type, variableSuffix) {
    window.api.copyImage([frame, type, variableSuffix]);
}

window.api.receive('filepath', (data) => {
    const variableName = 'F' + data[0] + data[2];
    variables[variableName] = 'generic_dynamic_ad/images/f' + data[0] + '_' + data[1]; 
    console.log(variableName);
    console.log(data);
    console.log(variables)
})

function exportAd() {
    let variableScript = 'var values=';
    const keys = Object.keys(variables).map(key => {
        return {name: key, value: variables[key]}
    });
    variableScript += JSON.stringify(keys);

    variableScript += `;

    var variables = {};
    var length = values.length;
    
    for(i = 0; i < length; i++) {
        var key = values[i].name;
        var value = values[i].value;
    
        variables[key]  = value;
    }`;

    window.api.send("toMain", variableScript);
}
