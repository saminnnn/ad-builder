const {contextBridge, ipcRenderer} = require("electron");

contextBridge.exposeInMainWorld(
    "api", {
        send: (channel, data) => {
            // whitelist channels
            let validChannels = ["toMain"];
            if (validChannels.includes(channel)) {
                ipcRenderer.send(channel, data);
            }
        },
        receive: (channel, func) => {
            console.log(channel);
            console.log(func);
            ipcRenderer.on(channel, (event, ...args) => func(...args));
            
        },

        copyImage: (data) => {
            ipcRenderer.send('copyFile', data)
        }
    }
);
